<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('articulos_model');
	}

	public function index(){
		$data['articulos'] = $this->articulos_model->lista_articulos();
		$data['title'] = 'Aplicacion Dinamica con CodeIgniter';
		$this->load->view('plantillas/front-end/header', $data);
		$this->load->view('plantillas/front-end/sidebar');
		$this->load->view('front-end/index');
		$this->load->view('plantillas/front-end/footer');

	}
//Function para mostrar articulo por ID
	/*public function detalle_articulo($id_articulo){
		$data['detalle'] = $this->articulos_model->detalle_articulo($id_articulo);
		$data['title'] = $data['detalle']->nombre_articulo;
		$this->load->view('detalle', $data);
	}*/
//Function para mostrar articulo por nombre de articulo
	/*public function detalle_articulo($nombre_articulo){
		$this->output->enable_profiler(TRUE);
		$nombre_limpio = str_replace('-', ' ', $nombre_articulo);
		$data['detalle'] = $this->articulos_model->detalle_articulo($nombre_limpio);
		$data['title'] = $data['detalle']->nombre_articulo;
		$this->load->view('detalle', $data);
	}*/
//Function para mostrar articulo por URL
	public function detalle_articulo($url_articulo){
		//$this->output->enable_profiler(TRUE);
		$url_limpio = $this->security->xss_clean($url_articulo);
		$data['detalle'] = $this->articulos_model->detalle_articulo($url_limpio);
		$data['title'] = $data['detalle']->nombre_articulo;
		$this->load->view('plantillas/front-end/header', $data);
		$this->load->view('plantillas/front-end/sidebar');
		$this->load->view('front-end/detalle');
		$this->load->view('plantillas/front-end/footer');
	}

	public function calendario(){
		$this->load->library('calendar');
		$data['title'] = 'Libreria Calendario';
		$data['contenido'] = 'contenido';
		$this->load->view('plantillas/front-end/header', $data);
		$this->load->view('plantillas/front-end/sidebar');
		$this->load->view('front-end/calendario');
		$this->load->view('plantillas/front-end/footer');

	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */