<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Model_blog');
	}

	public function index(){
		$data['title'] = 'Blog | Noticias';
		$this->load->view('plantillas/front-end/header', $data);
		$this->load->view('plantillas/front-end/sidebar');
		$this->load->view('front-end/view_blog_entrada');
		$this->load->view('plantillas/front-end/footer');
	}
	public function agregar(){
		$url_replace = url_title(convert_accented_characters(strtolower($this->input->post('nombre_articulo'))));
		$data = array(
			"nombre_articulo" => $this->input->post('nombre_articulo'),
			"contenido_articulo" => $this->input->post('contenido_articulo'),
			"fecha_articulo" => $this->input->post('fecha_articulo'),
			"url_articulo" => $url_replace,
			"id_categoria" => $this->input->post('id_categoria')
			);
		#print_r($data);
		$this->Model_blog->add_blog($data);
		redirect('home', 'refresh');
		
	}

}

/* End of file blog.php */
/* Location: ./application/controllers/blog.php */