<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wysiwyg extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Model_blog');
		$this->load->helper('url');
	}

	public function index(){
	$data['title'] ='WYSIWYG | Bootstrap';
	$this->load->view('front-end/view_wysiwyg', $data);
	}

	public function view_editor(){
		$url_replace = url_title(convert_accented_characters($this->input->post('nombre_articulo')), '-', TRUE);
		$data = array(
			"nombre_articulo" => $this->input->post('nombre_articulo'),
			"contenido_articulo" => $this->input->post('contenido_articulo'),
			"fecha_articulo" => $this->input->post('fecha_articulo'),
			"url_articulo" => $url_replace,
			"id_categoria" => $this->input->post('id_categoria')
			);
		#print_r($data);
		$this->Model_blog->add_blog($data);
		redirect('home', 'refresh');
	}

}

/* End of file wysiwyg.php */
/* Location: ./application/controllers/wysiwyg.php */