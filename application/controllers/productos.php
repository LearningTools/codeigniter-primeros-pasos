<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('productos_model');
		$this->load->library('cart');
	}

	public function lista_productos(){
		$data['productos'] = $this->productos_model->todos();
		$data['title'] = 'Listado de Productos';
		$this->load->view('plantillas/front-end/header', $data);
		$this->load->view('plantillas/front-end/sidebar');
		$this->load->view('front-end/lista_productos');
		$this->load->view('plantillas/front-end/footer');

	}//Function lista_productos

	public function agregar_carrito(){
		$opciones = array();
		if ($this->input->post('opciones')) {
			$opciones = $this->input->post('opciones');
		}
		$data = array(
			'id'  =>  $this->input->post('id'),
			'qty' =>  $this->input->post('cantidad'),
			'price'=> $this->input->post('precio'),
			'name' => $this->input->post('producto'),
			'options'=> $opciones
			);

		$this->cart->insert($data);
		redirect('productos/mostrar_carrito/');
	}//function agregar_productos

	public function mostrar_carrito(){
		$data['title'] = 'Listado de Productos';
		$this->load->view('plantillas/front-end/header', $data);
		$this->load->view('plantillas/front-end/sidebar');
		$this->load->view('front-end/carrito');
		$this->load->view('plantillas/front-end/footer');

	}//Function mostrar_carrito

	public function vaciar_carrito(){
		$this->cart->destroy();
		redirect('productos/mostrar_carrito/');
	
	}//Function Vaciar_carrito

	public function actualizar_carrito(){
		$rows = $this->input->post('rowid');
		$cantidades = $this->input->post('qty');
		$data = array();

		for ($i=0; $i < sizeof($rows); $i++) { 
			$data[] = array(
				'rowid' => $rows[$i],
				'qty' => $cantidades[$i]
				);
		}
		$this->cart->update($data);
		redirect('productos/mostrar_carrito');
		
	}//Function actualizar_carrito

	public function carrito_nuevo(){
		$data['productos'] = $this->productos_model->todos();
		$data['title'] = 'Carrusel Carro de Productos deslizante';
		$this->load->view('plantillas/front-end/header',$data);
		$this->load->view('plantillas/front-end/sidebar');
		$this->load->view('front-end/view_carrito_deslizante');
		$this->load->view('plantillas/front-end/footer');

	}

}//Class Productos

/* End of file productos.php */
/* Location: ./application/controllers/productos.php */