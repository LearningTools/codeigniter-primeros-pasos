<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Typehead extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}

	public function index(){
  	//$this->Offices_model->get();
  	$data['title'] = 'Plugin | TipeaheadJS';
  	//$data = $this->Offices_model->get();
  	$this->load->view('plantillas/front-end/header', $data);
  	$this->load->view('plantillas/front-end/sidebar');
  	$this->load->view('front-end/view_typehead');
  	$this->load->view('plantillas/front-end/footer');
  	//echo json_encode($data);
	}

	public function get_office(){
	$this->load->model('Offices_model');
    $data = $this->Offices_model->get();
    echo json_encode($data);

	}

}

/* End of file typehead.php */
/* Location: ./application/controllers/typehead.php */