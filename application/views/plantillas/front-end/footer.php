        <hr>

        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Company 2013</p>
                </div>
            </div>
        </footer>
    </div>
    <!-- /.container -->

    <!-- JavaScript -->
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
<script src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
<script src="http://eonasdan.github.io/bootstrap-datetimepicker/scripts/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url('assest/typeheadJS.main.js')?>"></script>
<!-- Script Calendar -->
 <script>
 $(function () {
    $('#datetimepicker1').datetimepicker();
    $('#myModal').modal();
});
 </script>

</body>

</html>