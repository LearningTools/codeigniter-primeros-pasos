<div class="container">
<div class="row">
	<div class="col-lg-8">
		<h2>Lista de Productos..</h2>
		<table class="table">
		          <thead>
		            <tr>
		              <th>#</th>
		              <th>Nombre</th>
		              <th>Precio</th>		   
		              <th>Cantidad</th>
		              <th>Opciones</th>
		              <th></th>
		            </tr>
		          </thead>
		          <tbody>
<?php foreach ($productos as $item){ ?>    	          	
<form action="<?php echo base_url(); ?>productos/agregar_carrito" method="post">
		            <tr>
		              <td><?php echo $item->id; ?>
		              <input type="hidden" name="id" value="<?php echo $item->id; ?>">
		         	  </td>
		              <td><?php echo $item->producto ?>
		              <input type="hidden" name="producto" value="<?php echo $item->producto; ?>">
		            </td>
		              <td><?php echo $item->precio ?><input type="hidden" name="precio" value="<?php echo $item->precio; ?>"></td>
		            <td>
		            	<input type="text" name="cantidad">
		            </td>
		            <td>
		            	<button type="submit" class="btn btn-success">Agregar</button>
		            </td>
		            </tr>		       
</form>		            
<?php } ?>
					
		          </tbody>
		        </table>
	</div>
<div class="row"></div>