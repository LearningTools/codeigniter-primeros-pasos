<div class="container">
<div class="row">
	<div class="col-lg-8">
		<a class="btn btn-info" href="<?php echo base_url();?>productos/lista_productos">Volver al Listado</a>
<form action="<?php echo base_url() ?>productos/actualizar_carrito" method="post">
	<table class="table">
	          <thead>
	            <tr>
	              <th>Nombre del Producto</th>
	              <th>Precio</th>
	              <th>Cantidad</th>
	              <th>Subtotal</th>
	            </tr>
	          </thead>
	          <tbody>
	          	<?php $i = 1; ?>
<?php foreach ($this->cart->contents() as $item) {
 ?>
 		<input type="hidden" name="rowid[]" value="<?php echo $item['rowid']; ?>">	        
	            <tr>
	              <td>
	              	<?php echo $item['name'];
	if ($this->cart->has_options($item['rowid']) == TRUE){ ?>
<p><?php foreach ($this->cart->product_options($item['rowid']) as $option_name => $option_value ) { ?>
<strong><?php echo $option_name; ?>:</strong>
<?php echo $option_value; ?><br>
<?php } //end foreach ?>
</p>
<?php }//end if ?>
	              </td>
	              <td>Pesos <?php echo number_format($item['price'], 0, '', '.') ?></td>
	              <td>
	              	<input type="text" name="qty[]" value="<?php echo $item['qty']; ?>">
	              </td>
	              <td>
	              	Pesos <?php echo number_format($item['subtotal'], 0, '', '.') ?>
	              </td>
	            </tr>	
		<?php $i++; ?>
		<?php }//end foreach ?>
				<tr>
					<td colspan="2">
						<button class="btn btn-primary" type="submit">Actualizar Carrito</button>
						<!-- <input class="btn btn-primary" type="submit" name="actualizar" value="Actualizar Carrito"> -->
						<a class="btn btn-warning" href="<?php echo base_url() ?>productos/vaciar_carrito">Vaciar Carro</a>
						
					</td>
					<td>Total :</td>
					<td>Pesos: <?php echo number_format($this->cart->total(),0,'', '.'); ?>
					</td>
				</tr>         
	          </tbody>
	        </table>
</form>