<div class="container">
        <div class="row">
            <div class="col-lg-8">
            	<?php 
                if (empty($articulos)) {
                    echo '<div class="alert alert-warning" role="alert"><strong>Hey!</strong> Sin Publicaciones en tu base de Datos :(
                        <p>
                        <a href=" '.base_url().'" class="btn btn-primary">Agregar Publicacion</a>
                        </p>
                        </div>';
                }
                else{
                    foreach ($articulos as $item) {
                    $uri = 'articulo/';
                    $uri .= url_title(convert_accented_characters($item->nombre_articulo), '-', TRUE);
                echo'<h1><a href="#">'.anchor($uri, $item->nombre_articulo).'</a>
                </h1>';
                echo ' <p class="lead">by <a href="#">WiikaLabs</a>
                </p>';
                echo '<span class="glyphicon glyphicon-time"></span> Posted on '.$item->fecha_articulo.'</p>';
                echo'<hr>';
                echo '<p>'.substr($item->contenido_articulo,0,200).'</p>
                <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>';
                echo '<hr>';
                }
                }
                
				?>
            </div>
        <div class="row"></div>
<?php //$this->benchmark_elapsed_time();?>