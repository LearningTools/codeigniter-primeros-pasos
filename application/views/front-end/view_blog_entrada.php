<div class="container">
<div class="row">
	<div class="col-lg-8">
		<h2>Ingresa Tu Titular de Noticia</h2>
</div>
<div class="row">
	<div class="col-lg-8">
		<div class="well">
			<form action="<?php echo base_url(); ?>blog/agregar" method="post">
			<div class="form-group">
    			<label for="noticia">Titulo de Articulo</label>
    			<input type="text" class="form-control" name="nombre_articulo" id="noticia" placeholder="Titulo Noticia">
  			</div>
  			<div class="form-group">
    			<label for="texto">Cuerpo Noticia</label>
    			<textarea class="form-control"  name="contenido_articulo" id="texto" placeholder="Redacta..."></textarea>
 			</div>
 			<div class="form-group">
 				<label for="">Fecha de Entrada</label>
				<div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control"   data-date-format="YYYY-MM-DD hh:mm:ss" name="fecha_articulo" />
                    <span class="input-group-addon"><i class="fa fa-calendar"></i>
                    </span>
                </div>        
 			</div>
 			<div class="form-group">
 				<label for="">Categoria</label>
 				<select name="id_categoria" class="form-control">
 					<option value="1">1</option>
 					<option value="2">2</option>
 				</select>
 			</div>
 			<div class="form-group">
                	<button class="btn btn-primary" type="submit">Guardar Nota</button>
            </div>
			</form>
		</div>
	</div>
</div>
