<div class="container">
    <div class="row">
        <div class="col-lg-8">
<?php foreach ($productos as $item) { ?>
            <div class="col-sm-4">
                <div class="col-item">
                    <div class="photo">
                        <img src="http://placehold.it/350x260" class="img-responsive" alt="a" />
                    </div>
                        <div class="info">
                            <div class="row">
                                <div class="price col-md-6">
                                    <h5><?php echo $item->producto; ?></h5>
                                    <h5 class="price-text-color"><?php echo $item->precio ?></h5>
                                </div>
                                <div class="rating hidden-sm col-md-6"></div>
                            </div>
                            <div class="separator clear-left">
                                <p class="btn-add">
                                    <i class="fa fa-shopping-cart"></i><a href="http://www.jquery2dotnet.com" class="hidden-sm">Add to cart</a></p>
                                <p class="btn-details">
                                    <i class="fa fa-list"></i><a class="hidden-sm" data-toggle="modal" href="<?php echo base_url()?>productos/click/<?php echo $item->id; ?>" data-target="#modal">More details</a>
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                </div>
            </div>
<?php } ?>
        </div>
    </div>
