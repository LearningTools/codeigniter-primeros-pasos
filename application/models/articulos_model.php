<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articulos_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function lista_articulos(){
		$this->db->order_by('id_articulo', 'desc');
		$consulta = $this->db->get('articuloscodeigniter');
		return $consulta->result();
	}
//Muestra articulo por ID
	/*function detalle_articulo($id_articulo){
		$this->db->where('id_articulo', $id_articulo);
		$consulta = $this->db->get('articuloscodeigniter');
		return $consulta->row();
	}*/
//Muestra articulo por nombre
	/*function detalle_articulo($nombre_articulo){
		$this->db->where('nombre_articulo', $nombre_articulo);
		$consulta = $this->db->get('articuloscodeigniter');
		return $consulta->row();

	}*/
	function detalle_articulo($url_articulo){
		$this->db->where('url_articulo', $url_articulo);
		$consulta = $this->db->get('articuloscodeigniter');
		return $consulta->row();
	}

}

/* End of file articulos_model.php */
/* Location: ./application/models/articulos_model.php */