<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aes_model extends CI_Model {
// en el archivo config.php agregar la siguiente linea
// $config['aes_encryption_key'] = '$U(U28J298_:-|mkmjhuiyf!KMInxns?djIO¿?)9UWE8Y7urdv';
	public $keycrypt;

	public function __construct(){
		parent::__construct();
		$this->keycrypt = $this->config->item("aes_encryption_key");
	}

	public function save($username,$password,$register){
		$user = $this->db->escape($username);
		$pass = $this->db->escape($password);
		$date = $this->db->escape($register);
		$this->db->set('username', "AES_ENCRYPT({$user},'$this->keycrypt')", FALSE);
		$this->db->set('password', "AES_ENCRYPT({$pass},'$this->keycrypt')", FALSE);
		$this->db->set('register_date', "AES_ENCRYPT({$date},'$this->keycrypt')", FALSE);
		$this->db->insert('users_aes');
	}

	public function get(){
		$query = $this->db->select("id,
			AES_DECRYPT(username,'$this->keycrypt') as username,
			AES_DECRYPT(password,'$this->keycrypt') as password,
			AES_DECRYPT(register_date,'$this->keycrypt') as register_date")
		->from("users_aes")
		->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
}

/* End of file aes_model.php */
/* Location: ./application/models/aes_model.php */