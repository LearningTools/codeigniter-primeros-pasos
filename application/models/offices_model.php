<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offices_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

  public function get() {
  	  $office = $this->input->post('search');
	  $this->db->select('Name');
	  $this->db->from('city');
	  $this->db->like('Name', $office);
	  $query = $this->db->get();

	  $office_array = array();
	  foreach ($query->result() as $row) {
	   $office_array[] = $row->Name;
	  }
	  $data = $office_array;

	  return $data;
  }
  	

}

/* End of file offices_model.php */
/* Location: ./application/models/offices_model.php */