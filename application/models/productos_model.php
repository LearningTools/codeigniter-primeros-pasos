<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productos_model extends CI_Model {

public function __construct(){
		parent::__construct();
		$this->load->database();
	}

public function todos(){
	$this->db->order_by('id', 'desc');
	$consulta = $this->db->get('productos');
	return $consulta->result();
}	

}

/* End of file productos_model.php */
/* Location: ./application/models/productos_model.php */